<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2012112900;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012112900;        // Requires this Moodle version
$plugin->component = 'profilefield_label'; // Full name of the plugin (used for diagnostics)
